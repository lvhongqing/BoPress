# ORM #

采用第三方ORM库SQLAlchemy，数据库连接方式请参阅配置章节。

    from bopress.orm import SessionFactory

	s = SessionFactory.session()
	e = SomeEntity()
	s.add(e)
	s.commit()
	a = s.query(SomeEntity).filter(SomeEntity.propname==1).all()
	...

定义数据表时继承`Entity`，在插件加载时能自动创建表，并使表名前缀保持一致。
    
    from bopress.orm import Entity

	class Options(Entity):
	    option_id = Column(BigInteger, autoincrement=True, nullable=False, primary_key=True)
	    option_name = Column(String(255), nullable=False, default="")
	    option_value = Column(PickleType, nullable=False)
	    autoload = Column(String(20), nullable=False, default="yes")

为保证表自动创建，最好把定义模型的模块在插件包`__init__.py`文件中导入。

> 在`bopress.orm`模块中定义了几个辅助创建表与表之间关联关系的函数，多对一，一对一，多对多。

## RAW SQL ##

不喜欢使用ORM，也可以直接使用Python DB-API2原生方式操作数据。

    from bopress.orm import SessionFactory
	cnn = SessionFactory.connect()
	c = cnn.cursor()
	c.callproc()
	c.execute()
	..
	c.close()
	cnn.close()
	
or

    SessionFactory.session().execute(sql)
    
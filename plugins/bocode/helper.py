# -*- coding: utf-8 -*-
__author__ = 'yezang'


class FormBuilder(object):
    @staticmethod
    def create(columns_, option, primary_key, exclude_columns):
        columns = list()
        for col in columns_:
            if col.name == primary_key or col.name in exclude_columns:
                continue
            columns.append(col)
        cols_num = int(option["column_nums"])
        fields_num = len(columns)
        if fields_num % cols_num == 0:
            num = int(fields_num / cols_num)
        else:
            num = int(fields_num / cols_num) + 1
        html_rows = '<div class="row">{}</div>' * num
        f = int(12 / cols_num)
        html_cols = ('<div class="col-xs-' + str(f) + '">{}</div>') * cols_num
        a = list()
        for i in range(num):
            a.append(html_cols)
        c = html_rows.format(*a)
        items = list()
        for col in columns:
            html = """
            <div class="form-group">
                    <label for="{name}">{name}</label>
                    <input type="{data_type}" class="form-control" id="{name}" name="{name}"
                           placeholder="{name}">
                </div>
            """.format(name=col.name, data_type=col.data_type)
            items.append(html)
        diff_num = num * cols_num - len(items)
        for i in range(diff_num):
            items.append("")

        r = c.format(*items)
        return r

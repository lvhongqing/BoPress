# -*- coding: utf-8 -*-

from bopress.hook import add_static_path, add_submenu_page, add_action

__author__ = 'yezang'


def scripts(res, current_screen):
    if current_screen.id == "gallery-demo":
        res.enqueue_plugin_script(src="gallerydemo/static/js/jquery.justifiedGallery.min.js", ver="3.6.3")


def styles(res, current_screen):
    if current_screen.id == "gallery-demo":
        res.enqueue_plugin_style(src="gallerydemo/static/css/justifiedGallery.min.css", ver="3.6.3")


def data(handler):
    items = [
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484233&di=ad6f50e0ca80c338cb520c5cc848aad9&imgtype=0&src=http%3A%2F%2Fb.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2F63d9f2d3572c11df28e42e30602762d0f703c2e8.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484232&di=474f6665703104b7a22557b8dd11ec71&imgtype=0&src=http%3A%2F%2Fb.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2F1f178a82b9014a90e7eb9d17ac773912b21bee47.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484222&di=eed990a9e4638b59ed84184f192beeb3&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201407%2F25%2F20140725132852_TxyaU.jpeg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484222&di=abf63e1afb37c8c752c743772f94787e&imgtype=0&src=http%3A%2F%2Fc.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2Ffaf2b2119313b07e6077d3bc0ad7912396dd8cb8.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484221&di=8e04fab45d616a2b4549151753ef6049&imgtype=0&src=http%3A%2F%2Fa.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2F960a304e251f95cab9a1ed00cf177f3e6709522b.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484219&di=3447039c387f8d20dc5f6f204140a1aa&imgtype=0&src=http%3A%2F%2Fpic65.nipic.com%2Ffile%2F20150419%2F8684504_205612692746_2.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484218&di=fc67d0ad92a9d19071fbdcf120d7b24c&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fpic%2Fitem%2Fc8177f3e6709c93dc9363e769f3df8dcd000546e.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484218&di=8bb0d82711fc5f679f1acb31c066961b&imgtype=0&src=http%3A%2F%2Ff.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2Fc9fcc3cec3fdfc03dfdfafcad23f8794a4c22618.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484217&di=d712b233e2b04471321f8b4f1b1e7d7a&imgtype=0&src=http%3A%2F%2Fwww.bz55.com%2Fuploads%2Fallimg%2F120712%2F1-120G2104G2.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484217&di=a4891530916c6cf6d18cd59827639b8a&imgtype=0&src=http%3A%2F%2Fimg3.duitang.com%2Fuploads%2Fitem%2F201504%2F11%2F20150411H1005_Gr4PZ.jpeg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484216&di=28a9a2ee95af96be41732df67b2e8e66&imgtype=0&src=http%3A%2F%2Fe.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2F0df3d7ca7bcb0a46fe7588d76a63f6246a60af8c.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484216&di=b2701ba7986f2aeb0098a7550ddc0785&imgtype=0&src=http%3A%2F%2Fd.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2F72f082025aafa40fe871b36bad64034f79f019d4.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484213&di=da675d6cd7568c3a0bfbe1e7468df582&imgtype=0&src=http%3A%2F%2Fg.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2Ff31fbe096b63f6249dc041108344ebf81a4ca300.jpg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474484202&di=8e828233065e836da8e11761ebcd2984&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201411%2F02%2F20141102213013_RuaFL.jpeg",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1489474563862&di=f324a5b8c24da85ded7c5bdba3423291&imgtype=0&src=http%3A%2F%2Fdesk.fd.zol-img.com.cn%2Ft_s960x600c5%2Fg5%2FM00%2F02%2F04%2FChMkJ1bKx26Ic6evABk6n_2v7L4AALHzgG_rhwAGTq3135.jpg"
    ]
    html = list()
    for v in items:
        h = """
        <a href="{0}"><img alt="相册样例" src="{0}"/></a>
        """.format(v, v)
        html.append(h)
    handler.render_json("".join(html))


add_static_path("gallerydemo/static")
add_submenu_page("dev-example", "Gallery Demo", "Gallery Demo", "gallery-demo", ["read"],
                 "gallerydemo/tpl/gallery.html")
add_action("bo_enqueue_styles", styles)
add_action("bo_enqueue_scripts", scripts)
add_action("bo_api_get_gallery_demo_data", data)

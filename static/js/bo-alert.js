function bo_alert_success(title, msg) {
    var html = '<div class="alert alert-success alert-dismissible">';
    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    html += '<h4><i class="icon fa fa-check"></i> ' + title + '</h4>';
    html += msg;
    html += '</div>';
    $("#bo_alert_wrapper").html(html);
}

function bo_alert_error(title, msg) {
    var html = '<div class="alert alert-danger alert-dismissible">';
    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    html += '<h4><i class="icon fa fa-ban"></i> ' + title + '</h4>';
    html += msg;
    html += '</div>';
    $("#bo_alert_wrapper").html(html);
}

function bo_alert_info(title, msg) {
    var html = '<div class="alert alert-info alert-dismissible">';
    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    html += '<h4><i class="icon fa fa-info"></i> ' + title + '</h4>';
    html += msg;
    html += '</div>';
    $("#bo_alert_wrapper").html(html);
}

function bo_alert_warning(title, msg) {
    var html = '<div class="alert alert-warning alert-dismissible">';
    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    html += '<h4><i class="icon fa fa-warning"></i> ' + title + '</h4>';
    html += msg;
    html += '</div>';
    $("#bo_alert_wrapper").html(html);
}

function bo_valid_errors(form_id, errors, message) {
    $("#" + form_id + " .form-group").removeClass("has-error");
    $("#" + form_id + " .help-block").remove();
    var has_err = false;
    for (var key in errors) {
        has_err = true;
        var parent = $('#' + form_id + ' input[name="' + key + '"]').parent();
        parent.addClass("has-error");
        var tip = '<span class="help-block">' + errors[key] + '</span>';
        parent.append(tip);
    }
    if (!has_err && message.length != 0) {
        alert(message);
    }
}